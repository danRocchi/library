
Library Project:

    This class system offers an easy way to rent and return books, cds and periodicals. These rentals keep track who rents from the library as well as how many copies of each product are currently available to be rented. 

Things to add:
    - Junit Testing to scripts
    - Method for searching things to rent by name
    - A turn in date for each rental + automatically charged late fees