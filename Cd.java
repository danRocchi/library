public class Cd extends Item{

    private String bandName;

    Cd(String name, String bandName, int available){
        this.itemName = name;
        this.bandName = bandName;
        this.available = available;
        this.rentable = true;
    }

    public void getInfo(){
        System.out.println("Album: " + itemName + "\nBand Name: " + bandName + "\nLeft in Stock: " + available);
    }
}