public class Renter {
    private int renterID;
    private String renterName;

    Renter(int renterID, String renterName){
        this.renterID = renterID;
        this.renterName = renterName;
    }

    public void getInfo(){
        System.out.println("RenterID: " + renterID + "\nName: " + renterName);
    }

    public String getName(){
        return renterName;
    }
}