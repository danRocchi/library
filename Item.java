public abstract class Item {

    protected String itemName;
    protected Renter renter;
    protected int available;
    protected boolean rentable;

    public Item rentItem(Renter requester){

        if(rentable == false){
            System.out.println("This Item is not available for rent");
            return null;
        }

        if (available >= 1){
            System.out.println(itemName + " has been successfully rented by " + requester.getName());
            renter = requester;
            available -= 1;
            return this;
        }else{
            System.out.println(itemName + " is currently rented, please check back later");
            return null;
        }
    }

    public void returnItem(){
        available += 1;
        renter = null;
    }

    public abstract void getInfo();
}