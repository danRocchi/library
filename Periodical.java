public class Periodical extends Item{
    private String publisher;


    Periodical(String name, String publisher, int available){
        this.itemName = name;
        this.publisher = publisher;
        this.available = available;
        this.rentable = false;
    }

    public void getInfo(){
        System.out.println("Periodical: " + itemName + "\nPublisher: " + publisher + "\nUnavailable to Rent");
    }
}