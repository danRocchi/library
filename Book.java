
public class Book extends Item{

    private int isbn;

    Book(String name, int isbn, int available){
        this.itemName = name;
        this.isbn = isbn;
        this.available = available;
        this.rentable = true;
    }

    public void getInfo(){
        System.out.println("Book Name: " + itemName + "\nISBN: " + isbn + "\nLeft in Stock: " + available);
    }
}