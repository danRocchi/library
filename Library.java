import java.util.HashMap;

public class Library
{
    public static void main(String[] args)
    {

        HashMap<Integer,Item> lib = new HashMap<Integer,Item>();

        Book book1 = new Book("Animal Farm", 12345, 0);
        Book book2 = new Book("Fellowship of the Ring", 23456, 2);
        Cd cd1 = new Cd("Christmas Island", "Ajj", 2);
        Cd cd2 = new Cd("Port of Morrow", "The Shins", 1);
        Periodical periodical1 = new Periodical("Vogue", "Some famous publisher", 3);
        Periodical periodical2 = new Periodical("Void", "Some other famous publisher", 3);

        Renter renter1 = new Renter(54321, "Paul Renters");
        Renter renter2 = new Renter(65432, "Mario Yoshison");


        lib.put(1, book1);
        lib.put(2, book2);
        lib.put(3, cd1);
        lib.put(4, cd2);
        lib.put(5, periodical1);
        lib.put(6, periodical2);

        lib.get(1).rentItem(renter1);
        lib.get(1).rentItem(renter1);
        lib.get(3).rentItem(renter2);
        lib.get(3).rentItem(renter1);
        lib.get(3).rentItem(renter2);
        lib.get(3).returnItem();
        lib.get(3).rentItem(renter1);
        lib.get(3).getInfo();
        lib.get(5).rentItem(renter1);
        lib.get(6).getInfo();
    
    }
}
